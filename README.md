# README #

### About ###
XInput wrapper for locally singleplayer games. Fixes on the fly device detection for
apps that only scan devices during startup. Only one device is supported at the moment,
the wrapper will _always_ report back exactly one device, even if it is not connected.

### Build ###
Use build.bat from a development command prompt. Binaries will be in build subfolder.
### Dependencies ###
XInput 1.3 is required to be present
### Usage ###
* Copy xinput1_3.dll from build subfolder next to the application .exe
* Because the .dll load ordering your application _should_ now use this wrapper.

To remove it, simply delete the copied xinput1_3.dll from the application folder.

NOTE : if you or the application already uses a custom xinput1_3.dll dont forget to back it up
first.
