#include "config.h"
#include <stdio.h>

char* IniFind(char* ini, char* key)
{
    char* result = strstr(ini, key);
    if (result)
    {
        result = strstr(result, "=");
        if (result)
        {
            ++result;//Skip =
        }
    }
    return result;
}

bool IniReadI32(char* ini, i32* result)
{
    return (sscanf(ini, "%d", result) == 1);
}

void IniGetValueI32(char* ini, char* key, i32* value)
{
    if (char* v = IniFind(ini, key))
    {
        IniReadI32(v, value);
    }
}

bool IniReadF32(char* ini, f32* result)
{
    return (sscanf(ini, "%f", result) == 1);
}

void IniGetValueF32(char* ini, char* key, f32* value)
{
    if (char* v = IniFind(ini, key))
    {
        IniReadF32(v, value);
    }
}

Config::Status ConfigLoadFromFile(Config* config, char* path)
{
    Config::Status status = Config::STATUS_OK;
    //NOTE : Load defaults so if we fail config load we still have something
    config->logMode = Config::LOG_OFF;
    config->inputMode = Config::INPUT_MODE_XINPUT;
    config->inputLGripSource = Config::FF_SOURCE_LEFT;
    config->inputRGripSource = Config::FF_SOURCE_RIGHT;
    config->inputLTriggerSource = Config::FF_SOURCE_LEFT;
    config->inputRTriggerSource = Config::FF_SOURCE_RIGHT;
    config->inputLGripStr = 1.0f;
    config->inputRGripStr = 1.0f;
    config->inputLTriggerStr = 0.0f;
    config->inputRTriggerStr = 0.0f;

    FILE* file = fopen(path, "rb");
    if (file)
    {
        fseek(file, 0, SEEK_END);
        msize size = ftell(file);
        fseek(file, 0, SEEK_SET);
        char* ini = (char*) malloc(size);
        if (ini)
        {
            fread(ini, sizeof(char), size, file);

            IniGetValueI32(ini, "log_mode = ", (i32*)&config->logMode);
            IniGetValueI32(ini, "input_mode = ", (i32*)&config->inputMode);
            IniGetValueI32(ini, "input_ff_source_lgrip = ", (i32*)&config->inputLGripSource);
            IniGetValueI32(ini, "input_ff_source_rgrip = ", (i32*)&config->inputRGripSource);
            IniGetValueI32(ini, "input_ff_source_ltrigger = ", (i32*)&config->inputLTriggerSource);
            IniGetValueI32(ini, "input_ff_source_rtrigger = ", (i32*)&config->inputRTriggerSource);
            IniGetValueF32(ini, "input_ff_strength_lgrip = ", &config->inputLGripStr);
            IniGetValueF32(ini, "input_ff_strength_rgrip = ", &config->inputRGripStr);
            IniGetValueF32(ini, "input_ff_strength_ltrigger = ", &config->inputLTriggerStr);
            IniGetValueF32(ini, "input_ff_strength_rtrigger = ", &config->inputRTriggerStr);

            free(ini);
        }
        else
        {
            status = Config::STATUS_ERROR_MEMORY;
        }

        fclose(file);
    }
    else
    {
        status = Config::STATUS_ERROR_FILE;
    }

    return status;
}