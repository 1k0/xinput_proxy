#pragma once

#include "common.h"

struct Config
{
    enum FFSource : u32
    {
        FF_SOURCE_LEFT = 0,
        FF_SOURCE_RIGHT = 1
    };

    enum InputMode : u32
    {
        INPUT_MODE_NONE = 0,
        INPUT_MODE_XINPUT = 1,
        INPUT_MODE_WINDOWS_GAMING_INPUT = 2,
    };

    enum LogMode : i32
    {
        LOG_OFF = 0,
        LOG_CONSOLE = 1,
        LOG_FILE = 2,
    };

    enum Status : u32
    {
        STATUS_OK = 0,
        STATUS_ERROR_MEMORY = 1,
        STATUS_ERROR_FILE = 2,
    };

    LogMode logMode;
    InputMode inputMode;

    FFSource inputLGripSource;
    FFSource inputRGripSource;
    FFSource inputLTriggerSource;
    FFSource inputRTriggerSource;
    f32 inputLGripStr;
    f32 inputRGripStr;
    f32 inputLTriggerStr;
    f32 inputRTriggerStr;
};

Config::Status ConfigLoadFromFile(Config* config, char* path);
