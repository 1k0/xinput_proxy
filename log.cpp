#include "log.h"

static LogLevel gLogLevel;
static LogTarget gLogTarget;
static HANDLE gLogHandle;
static BOOL gConsoleOwned;

void LogInit(LogTarget target, char* path)
{
    gLogLevel = LOG_LEVEL_DEBUG;
    gLogTarget = LOG_TARGET_NONE;
    gLogHandle = INVALID_HANDLE_VALUE;
    gConsoleOwned = FALSE;

    switch (target)
    {
    default:
    case LOG_TARGET_NONE:
    {
        gLogTarget = LOG_TARGET_NONE;
    }
    break;
    case LOG_TARGET_CONSOLE:
    {
        gConsoleOwned = AllocConsole();
        gLogHandle = CreateFileA(
            "CONOUT$",
            GENERIC_READ | GENERIC_WRITE,
            FILE_SHARE_READ,
            0,
            OPEN_EXISTING,
            0,
            0
        );
        gLogTarget = (gLogHandle == INVALID_HANDLE_VALUE) ? LOG_TARGET_NONE : LOG_TARGET_CONSOLE;
    }
    break;
    case LOG_TARGET_FILE:
    {
        gLogHandle = CreateFileA(
            path,
            GENERIC_WRITE,
            0,
            NULL,
            CREATE_ALWAYS,
            FILE_ATTRIBUTE_NORMAL,
            NULL
        );
        gLogTarget = (gLogHandle == INVALID_HANDLE_VALUE) ? LOG_TARGET_NONE : LOG_TARGET_CONSOLE;
    }
    break;
    case LOG_TARGET_DEBUGGER:
    {
        gLogTarget = (IsDebuggerPresent()) ? LOG_TARGET_DEBUGGER : LOG_TARGET_NONE;
    }
    break;
    }
}

void LogFinish()
{
    CloseHandle(gLogHandle);
    if (gConsoleOwned)
    {
        FreeConsole();
    }
}

void LogSetLevel(LogLevel level)
{
    gLogLevel = level;
}

void Log(LogLevel level, const char* format, ...)
{
    va_list args;
    va_start(args, format);

    if (gLogTarget && (level <= gLogLevel))
    {
        char buffer[4 * 1024];
        DWORD bufferWritten = vsnprintf(buffer, sizeof(buffer), format, args);

        if (gLogTarget == LOG_TARGET_DEBUGGER)
        {
            OutputDebugStringA(buffer);
        }
        else
        {
            DWORD fileWritten = 0;
            WriteFile(gLogHandle, buffer, bufferWritten, &fileWritten, NULL);
        }
    }

    va_end(args);
}