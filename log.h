#pragma once

#include "common.h"

enum LogLevel : u32
{
    LOG_LEVEL_OFF = 0,
    LOG_LEVEL_ERROR,
    LOG_LEVEL_WARNING,
    LOG_LEVEL_INFO,
    LOG_LEVEL_DEBUG,
};

enum LogTarget : u32
{
    LOG_TARGET_NONE = 0,
    LOG_TARGET_CONSOLE,
    LOG_TARGET_FILE,
    LOG_TARGET_DEBUGGER,
};

void LogInit(LogTarget target, char* path = NULL);
void LogFinish();
void LogSetLevel(LogLevel level);
void Log(LogLevel level, const char* format, ...);

#define LOG_E(format, ...) Log(LOG_LEVEL_ERROR, "[E]"##format##"\n", __VA_ARGS__)
#define LOG_W(format, ...) Log(LOG_LEVEL_WARNING, "[W]"##format##"\n", __VA_ARGS__)
#define LOG_I(format, ...) Log(LOG_LEVEL_INFO, "[I]"##format##"\n", __VA_ARGS__)
#define LOG_D(format, ...) Log(LOG_LEVEL_DEBUG, "[D]"##format##"\n", __VA_ARGS__)