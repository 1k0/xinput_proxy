#include "common.h"
#include "log.h"
#include "config.h"
#include "xxinput.h"
#include "xoinput.h"

struct InputWindow
{
    const char* windowClassName;
    WNDCLASSEX windowClass;
    HWND window;
    HANDLE thread;
    bool keepRunning;
};
static InputWindow gInputWindow = {};
static Config gConfig = {};
static INIT_ONCE gInitOnce = INIT_ONCE_STATIC_INIT;

#define FLAG_CLEAR ((LONG) 0)
#define FLAG_SET ((LONG) 1)
static volatile LONG gFlagScanForControllers = FLAG_CLEAR;

static LRESULT InputWindowProc(HWND handle, UINT msg, WPARAM wparam, LPARAM lparam)
{
    //NOTE : we wont get WM_DEVICECHANGE for initially connected controllers, so
    //scan in WM_CREATE too.
    if (msg == WM_CREATE || msg == WM_DEVICECHANGE)
    {
        if (gConfig.inputMode == Config::INPUT_MODE_XINPUT)
        {
            InterlockedExchange(&gFlagScanForControllers, FLAG_SET);
        }

        return 0;
    }

    return DefWindowProc(handle, msg, wparam, lparam);
}

static DWORD InputWindowThreadProc(LPVOID param)
{
    BOOL status;
    MSG message = {};
    while (gInputWindow.keepRunning)
    {
        while ((status = GetMessage(&message, NULL, 0, 0)) != 0)
        {
            if (status != -1)   //NOTE : -1 is ERROR
            {
                TranslateMessage(&message);
                DispatchMessage(&message);
            }
        }
    }
    return 0;
}

static InputWindow InputWindowInit()
{
    InputWindow input = {};

    input.windowClassName = "WND_CLASS_DUMMY";

    input.windowClass = {};
    input.windowClass.cbSize = sizeof(WNDCLASSEX);
    input.windowClass.lpfnWndProc = InputWindowProc;
    input.windowClass.hInstance = GetModuleHandle(NULL);
    input.windowClass.lpszClassName = input.windowClassName;
    //NOTE : dont really care if this succeeds, if not window creation will fail anyways
    RegisterClassEx(&input.windowClass);
    input.window = CreateWindowEx(WS_EX_NOACTIVATE, input.windowClassName, "DummyWindow", 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);
    if (input.window)
    {
        input.thread = CreateThread(NULL, 0, InputWindowThreadProc, 0, 0, 0);
        if (input.thread)
        {
            input.keepRunning = true;
        }
    }

    return input;
}

BOOL CALLBACK AppInit(PINIT_ONCE InitOnce, PVOID Parameter, PVOID *lpContext)
{
    Config::Status configStatus = ConfigLoadFromFile(&gConfig, "xinput1_3_config.ini");
    LogInit((LogTarget) gConfig.logMode, "xinput1_3_log.txt");
    LOG_I("[APP] Init");
    if (configStatus)
    {
        LOG_W("[APP] Config loading error : %d, defaults loaded", configStatus);
    }
    else
    {
        LOG_I("[APP] Config loaded");
    }

    if (gConfig.inputMode == Config::INPUT_MODE_XINPUT)
    {
        LOG_I("[APP] Init with mode XINPUT");
        XOInputInit();
        gInputWindow = InputWindowInit();
    }
    else if (gConfig.inputMode == Config::INPUT_MODE_WINDOWS_GAMING_INPUT)
    {
        LOG_I("[APP] Init with mode WINDOWS_GAMING_INPUT");
        gConfig.inputMode = XXInputInit() ? (Config::INPUT_MODE_WINDOWS_GAMING_INPUT) : (Config::INPUT_MODE_NONE);
    }

    return TRUE;
}
#define APP_INIT_ONCE() InitOnceExecuteOnce(&gInitOnce, AppInit, NULL, NULL)

void AppUpdate()
{
    APP_INIT_ONCE();

    LONG flagScan = InterlockedExchange(&gFlagScanForControllers, FLAG_CLEAR);
    if (flagScan && gConfig.inputMode == Config::INPUT_MODE_XINPUT)
    {
        LOG_I("[APP] Update scan flag");
        XOInputScanForDevices();
    }
}

void AppFinish()
{
    LOG_I("[APP] Finish");
    LogFinish(); //have to call it to flush it
}

BOOL WINAPI DllMain(HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
    switch (dwReason)
    {
        //NOTE : "When DLL_PROCESS_DETACH tells you that the process is exiting, 
        //your best bet is just to return without doing anything"
        //-Raymond Chen
        case DLL_PROCESS_DETACH :
        {
            LOG_I("[APP] DLL detach event");
            AppFinish();
        }
        break;
    }
    return TRUE;
}

extern "C"
{

DWORD WINAPI API_XInputGetState(DWORD idx, XINPUT_STATE* state)
{
    AppUpdate();

    //Allow only one controller at index 0, so we can lie about presence
    if (gConfig.inputMode && idx == 0)
    {
        if (gConfig.inputMode == Config::INPUT_MODE_XINPUT)
        {
            for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
            {
                if (XOInputGetState(i, state) == PRESENCE_CONNECTED)
                {
                    return PRESENCE_CONNECTED;
                }
            }

            (*state) = {};
            return PRESENCE_CONNECTED;
        }
        else if (gConfig.inputMode == Config::INPUT_MODE_WINDOWS_GAMING_INPUT)
        {
            for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
            {
                if (XXInputGetState(i, state) == PRESENCE_CONNECTED)
                {
                    return PRESENCE_CONNECTED;
                }
            }

            (*state) = {};
            return PRESENCE_CONNECTED;
        }
    }

    return PRESENCE_DISCONNECTED;
}

DWORD WINAPI API_XInputSetState(DWORD idx, XINPUT_VIBRATION* vibration)
{
    AppUpdate();
    
    if (gConfig.inputMode && idx == 0)
    {
        if (gConfig.inputMode == Config::INPUT_MODE_XINPUT)
        {
            for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
            {
                if (XOInputSetState(idx, vibration) == PRESENCE_CONNECTED)
                {
                    return PRESENCE_CONNECTED;
                }
            }

            return PRESENCE_CONNECTED;
        }
        else if (gConfig.inputMode == Config::INPUT_MODE_WINDOWS_GAMING_INPUT)
        {
            for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
            {
                XINPUT_VIBRATION grip;
                XINPUT_VIBRATION trigger;
                grip.wLeftMotorSpeed = (gConfig.inputLGripSource == Config::FF_SOURCE_LEFT) ? (vibration->wLeftMotorSpeed) : (vibration->wRightMotorSpeed);
                grip.wLeftMotorSpeed *= gConfig.inputLGripStr;
                grip.wRightMotorSpeed = (gConfig.inputRGripSource == Config::FF_SOURCE_LEFT) ? (vibration->wLeftMotorSpeed) : (vibration->wRightMotorSpeed);
                grip.wRightMotorSpeed *= gConfig.inputRGripStr;
                trigger.wLeftMotorSpeed = (gConfig.inputLTriggerSource == Config::FF_SOURCE_LEFT) ? (vibration->wLeftMotorSpeed) : (vibration->wRightMotorSpeed);
                trigger.wLeftMotorSpeed *= gConfig.inputLTriggerStr;
                trigger.wRightMotorSpeed = (gConfig.inputRTriggerSource == Config::FF_SOURCE_LEFT) ? (vibration->wLeftMotorSpeed) : (vibration->wRightMotorSpeed);
                trigger.wRightMotorSpeed *= gConfig.inputRTriggerStr;

                if (XXInputSetState(idx, &grip, &trigger) == PRESENCE_CONNECTED)
                {
                    return PRESENCE_CONNECTED;
                }
            }

            return PRESENCE_CONNECTED;
        }
    }
    
    return PRESENCE_DISCONNECTED;
}

DWORD WINAPI API_XInputGetCapabilities(DWORD idx, DWORD flags, XINPUT_CAPABILITIES* caps)
{
    AppUpdate();

    if (gConfig.inputMode && idx == 0)
    {
        if (gConfig.inputMode == Config::INPUT_MODE_XINPUT)
        {
            for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
            {
                if (XOInputGetCapabilities(idx, flags, caps) == PRESENCE_CONNECTED)
                {
                    return PRESENCE_CONNECTED;
                }
            }

            (*caps) = {};
            return PRESENCE_CONNECTED;
        }
        else if (gConfig.inputMode == Config::INPUT_MODE_WINDOWS_GAMING_INPUT)
        {
            for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
            {
                if (XXInputGetCapabilities(idx, flags, caps) == PRESENCE_CONNECTED)
                {
                    return PRESENCE_CONNECTED;
                }
            }

            (*caps) = {};
            return PRESENCE_CONNECTED;
        }
    }

    return PRESENCE_DISCONNECTED;
}

void WINAPI API_XInputEnable(BOOL enable)
{
    AppUpdate();
}

DWORD WINAPI API_XInputGetDSoundAudioDeviceGuids(DWORD idx, GUID* pDSoundRenderGuid, GUID* pDSoundCaptureGuid)
{
    AppUpdate();

    if (gConfig.inputMode && idx == 0)
    {
        pDSoundRenderGuid = NULL;
        pDSoundCaptureGuid = NULL;
        return PRESENCE_CONNECTED;
    }

    return PRESENCE_DISCONNECTED;
}

DWORD WINAPI API_XInputGetBatteryInformation(DWORD idx, BYTE devType, XINPUT_BATTERY_INFORMATION* pBatteryInformation)
{
    AppUpdate();

    if (gConfig.inputMode && idx == 0)
    {
        pBatteryInformation->BatteryType = BATTERY_TYPE_NIMH;
        pBatteryInformation->BatteryLevel = BATTERY_LEVEL_FULL;
        return PRESENCE_CONNECTED;
    }

    return PRESENCE_DISCONNECTED;
}

DWORD WINAPI API_XInputGetKeystroke(DWORD idx, DWORD dwReserved, PXINPUT_KEYSTROKE pKeystroke)
{
    AppUpdate();

    if (gConfig.inputMode && idx == 0)
    {
        (*pKeystroke) = {};
        return ERROR_EMPTY;
    }

    return PRESENCE_DISCONNECTED;
}

DWORD WINAPI API_XInputGetStateEx(DWORD idx, XINPUT_STATE* state)
{
    return API_XInputGetState(idx, state);
}

DWORD WINAPI API_XInputWaitForGuideButton(DWORD idx, DWORD dwFlag, LPVOID pVoid)
{
    AppUpdate();

    if (gConfig.inputMode && idx == 0)
    {
        return PRESENCE_CONNECTED;
    }

    return PRESENCE_DISCONNECTED;
}

DWORD WINAPI API_XInputCancelGuideButtonWait(DWORD idx)
{
    AppUpdate();

    if (gConfig.inputMode && idx == 0)
    {
        return PRESENCE_CONNECTED;
    }

    return PRESENCE_DISCONNECTED;
}

DWORD WINAPI API_XInputPowerOffController(DWORD idx)
{
    AppUpdate();

    if (gConfig.inputMode && idx == 0)
    {
        return PRESENCE_CONNECTED;
    }

    return PRESENCE_DISCONNECTED;
}

}