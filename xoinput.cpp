#include "xoinput.h"
#include "log.h"

static DWORD presence[XUSER_MAX_COUNT];

void XOInputInit()
{
    for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
    {
        presence[i] = PRESENCE_DISCONNECTED;
    }
}

void XOInputScanForDevices()
{
    LOG_I("[XOInput] ScanForDevices");
    for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
    {
        XINPUT_STATE state;
        presence[i] = XInputGetState(i, &state);
        LOG_I("[XOInput] device : %d : %d", i, (presence[i] == PRESENCE_CONNECTED));
    }
}

DWORD XOInputGetState(DWORD index, XINPUT_STATE* state)
{
    if (presence[index] == PRESENCE_CONNECTED)
    {
        return XInputGetState(index, state);
    }
    return PRESENCE_DISCONNECTED;
}

DWORD XOInputSetState(DWORD index, XINPUT_VIBRATION* vib)
{
    if (presence[index] == PRESENCE_CONNECTED)
    {
        return XInputSetState(index, vib);
    }
    return PRESENCE_DISCONNECTED;
}

DWORD XOInputGetCapabilities(DWORD index, DWORD flags, XINPUT_CAPABILITIES* caps)
{
    if (presence[index] == PRESENCE_CONNECTED)
    {
        return XInputGetCapabilities(index, flags, caps);
    }
    return PRESENCE_DISCONNECTED;
}