#pragma once

#include "common.h"
#include <xinput.h>

void XOInputInit();
void XOInputScanForDevices();

DWORD XOInputGetState(DWORD index, XINPUT_STATE* state);
DWORD XOInputSetState(DWORD index, XINPUT_VIBRATION* vib);
DWORD XOInputGetCapabilities(DWORD index, DWORD flags, XINPUT_CAPABILITIES* caps);