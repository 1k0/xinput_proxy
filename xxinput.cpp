#include "xxinput.h"
#include "common.h"
#include "log.h"

#include <roapi.h>
#include <wrl.h>
#include <windows.gaming.input.h>

using namespace ABI::Windows::Foundation::Collections;
using namespace ABI::Windows::Gaming::Input;
using namespace Microsoft::WRL;
using namespace Microsoft::WRL::Wrappers;

struct XXInput
{
    IGamepadStatics* gamepadManager;
    EventRegistrationToken gamepadAdded;
    EventRegistrationToken gamepadRemoved;
    CRITICAL_SECTION gamepadsLock;
    IGamepad* gamepads[XUSER_MAX_COUNT];
    DWORD packetNbrs[XUSER_MAX_COUNT];
};
static XXInput xxInput;

static HRESULT XXInputOnGamepadAdded(IInspectable* ins, IGamepad* gamepad);
static HRESULT XXInputOnGamepadRemoved(IInspectable* ins, IGamepad* gamepad);

bool XXInputInit()
{
    HRESULT status = RoInitialize(RO_INIT_MULTITHREADED);
    if (RoGetActivationFactory(
        HStringReference(RuntimeClass_Windows_Gaming_Input_Gamepad).Get(),
        __uuidof(IGamepadStatics),
        (void**)&xxInput.gamepadManager
    ) == S_OK)
    {
        LOG_I("[XXInput] WinRT initialized, IGamepadStatics found");
        //NOTE : you reeeeeally dont want to mess with this just keep it. Got lambda and all that shit underneath
        //its best to leave it in the modern cpp way.
        status = xxInput.gamepadManager->add_GamepadAdded(
            Callback<__FIEventHandler_1_Windows__CGaming__CInput__CGamepad>(XXInputOnGamepadAdded).Get(),
            &xxInput.gamepadAdded
        );
        status = xxInput.gamepadManager->add_GamepadRemoved(
            Callback<__FIEventHandler_1_Windows__CGaming__CInput__CGamepad>(XXInputOnGamepadRemoved).Get(),
            &xxInput.gamepadRemoved
        );

        InitializeCriticalSection(&xxInput.gamepadsLock);
        memset(xxInput.gamepads, 0x00, sizeof(xxInput.gamepads));
        memset(xxInput.packetNbrs, 0x00, sizeof(xxInput.packetNbrs));
    }
    else
    {
        LOG_E("[XXInput] error IGamepadStatics not found");
        xxInput.gamepadManager = NULL;
        RoUninitialize();
        return false;
    }

    return true;
}

static HRESULT XXInputOnGamepadAdded(IInspectable* ins, IGamepad* gamepad)
{
    LOG_I("[XXInput] gamepad added event");

    EnterCriticalSection(&xxInput.gamepadsLock);
    u32 i = 0;
    for (i = 0; i < XUSER_MAX_COUNT; ++i)
    {
        if (xxInput.gamepads[i] == NULL)
        {
            break;
        }
    }

    if (i < XUSER_MAX_COUNT)
    {
        gamepad->AddRef();
        xxInput.gamepads[i] = gamepad;
        xxInput.packetNbrs[i] = 0;
    }
    LeaveCriticalSection(&xxInput.gamepadsLock);

    return S_OK;
}

static HRESULT XXInputOnGamepadRemoved(IInspectable* ins, IGamepad* gamepad)
{
    LOG_I("[XXInput] gamepad removed event");

    EnterCriticalSection(&xxInput.gamepadsLock);
    for (u32 i = 0; i < XUSER_MAX_COUNT; ++i)
    {
        if (xxInput.gamepads[i] == gamepad)
        {
            gamepad->Release();
            xxInput.gamepads[i] = NULL;
        }
    }
    LeaveCriticalSection(&xxInput.gamepadsLock);

    return S_OK;
}

static IGamepad* XXInputGetGamepad(DWORD index)
{
    IGamepad* gamepad = NULL;

    if (xxInput.gamepadManager && index < XUSER_MAX_COUNT)
    {
        EnterCriticalSection(&xxInput.gamepadsLock);
        gamepad = xxInput.gamepads[index];
        if (gamepad) gamepad->AddRef();
        LeaveCriticalSection(&xxInput.gamepadsLock);
    }

    return gamepad;
}

#define XXINPUT_BUTTON_GROUP_MASK_MENU_VIEW ( GamepadButtons_Menu | GamepadButtons_View )
#define XXINPUT_BUTTON_GROUP_MASK_ABXY ( GamepadButtons_A | GamepadButtons_B | GamepadButtons_X | GamepadButtons_Y )
#define XXINPUT_BUTTON_GROUP_MASK_DPAD ( GamepadButtons_DPadUp | GamepadButtons_DPadDown | GamepadButtons_DPadLeft | GamepadButtons_DPadRight )
#define XXINPUT_BUTTON_GROUP_MASK_SHOULDER ( GamepadButtons_LeftShoulder | GamepadButtons_RightShoulder )
#define XXINPUT_BUTTON_GROUP_MASK_THUMB  ( GamepadButtons_LeftThumbstick | GamepadButtons_RightThumbstick )

DWORD XXInputGetState(DWORD index, XINPUT_STATE* state)
{
    DWORD status = PRESENCE_CONNECTED;
    (*state) = {};

    IGamepad* gamepad = XXInputGetGamepad(index);
    if (gamepad)
    {
        GamepadReading gamepadState;
        if (gamepad->GetCurrentReading(&gamepadState) == S_OK)
        {
            state->dwPacketNumber = xxInput.packetNbrs[index]++;
            state->Gamepad.bLeftTrigger = (BYTE)(gamepadState.LeftTrigger * 255.0);
            state->Gamepad.bRightTrigger = (BYTE)(gamepadState.RightTrigger * 255.0);
            //TODO : do it properly? -32768 cannot be hit ATM
            state->Gamepad.sThumbLX = (SHORT)(gamepadState.LeftThumbstickX * 32767.0);
            state->Gamepad.sThumbLY = (SHORT)(gamepadState.LeftThumbstickY * 32767.0);
            state->Gamepad.sThumbRX = (SHORT)(gamepadState.RightThumbstickX * 32767.0);
            state->Gamepad.sThumbRY = (SHORT)(gamepadState.RightThumbstickY * 32767.0);
            state->Gamepad.wButtons = 0;
            //Bits 0, 1 to 4, 5
            state->Gamepad.wButtons |= (gamepadState.Buttons & XXINPUT_BUTTON_GROUP_MASK_MENU_VIEW) << 4;
            //Bits 2, 3, 4, 5 to 12, 13, 14, 15
            state->Gamepad.wButtons |= (gamepadState.Buttons & XXINPUT_BUTTON_GROUP_MASK_ABXY) << 10;
            //Bits 6, 7, 8, 9 to 0, 1, 2, 3 
            state->Gamepad.wButtons |= (gamepadState.Buttons & XXINPUT_BUTTON_GROUP_MASK_DPAD) >> 6;
            //Bits 10, 11, to 8, 9
            state->Gamepad.wButtons |= (gamepadState.Buttons & XXINPUT_BUTTON_GROUP_MASK_SHOULDER) >> 2;
            //Bits 12, 13 to 6, 7
            state->Gamepad.wButtons |= (gamepadState.Buttons & XXINPUT_BUTTON_GROUP_MASK_THUMB) >> 6;
        }

        gamepad->Release();
    }
    else
    {
        status = PRESENCE_DISCONNECTED;
    }

    return status;
}

DWORD XXInputSetState(DWORD index, XINPUT_VIBRATION* grip, XINPUT_VIBRATION* trigger)
{
    DWORD status = PRESENCE_CONNECTED;

    IGamepad* gamepad = XXInputGetGamepad(index);
    if (gamepad)
    {
        GamepadVibration vib = {};
        vib.LeftMotor = grip->wLeftMotorSpeed / 65535.0;
        vib.RightMotor = grip->wRightMotorSpeed / 65535.0;
        vib.LeftTrigger = (trigger) ? (trigger->wLeftMotorSpeed / 65535.0) : (0.0);
        vib.RightTrigger = (trigger) ? (trigger->wRightMotorSpeed / 65535.0) : (0.0);

        gamepad->put_Vibration(vib);

        gamepad->Release();
    }
    else
    {
        status = PRESENCE_DISCONNECTED;
    }

    return status;
}

DWORD XXInputGetCapabilities(DWORD index, DWORD flags, XINPUT_CAPABILITIES* capabilities)
{
    DWORD status = PRESENCE_CONNECTED;
    (*capabilities) = {};

    IGamepad* gamepad = XXInputGetGamepad(index);
    if (gamepad)
    {
        capabilities->Type = XINPUT_DEVTYPE_GAMEPAD;
        capabilities->SubType = XINPUT_DEVSUBTYPE_GAMEPAD;
        capabilities->Flags = XINPUT_CAPS_FFB_SUPPORTED;
        capabilities->Gamepad.bLeftTrigger = 255;
        capabilities->Gamepad.bRightTrigger = 255;
        capabilities->Gamepad.sThumbLX = 65535;
        capabilities->Gamepad.sThumbRX = 65535;
        capabilities->Gamepad.sThumbLY = 65535;
        capabilities->Gamepad.sThumbRY = 65535;
        capabilities->Gamepad.wButtons =
            XINPUT_GAMEPAD_DPAD_UP |
            XINPUT_GAMEPAD_DPAD_DOWN |
            XINPUT_GAMEPAD_DPAD_LEFT |
            XINPUT_GAMEPAD_DPAD_RIGHT |
            XINPUT_GAMEPAD_START |
            XINPUT_GAMEPAD_BACK |
            XINPUT_GAMEPAD_LEFT_THUMB |
            XINPUT_GAMEPAD_RIGHT_THUMB |
            XINPUT_GAMEPAD_LEFT_SHOULDER |
            XINPUT_GAMEPAD_RIGHT_SHOULDER |
            XINPUT_GAMEPAD_A |
            XINPUT_GAMEPAD_B |
            XINPUT_GAMEPAD_X |
            XINPUT_GAMEPAD_Y;
        capabilities->Vibration.wLeftMotorSpeed = 65535;
        capabilities->Vibration.wRightMotorSpeed = 65535;

        gamepad->Release();
    }
    else
    {
        status = PRESENCE_DISCONNECTED;
    }

    return status;
}