#pragma once

#include "common.h"
#include <xinput.h>

bool XXInputInit();

DWORD XXInputSetState(DWORD index, XINPUT_VIBRATION* grip, XINPUT_VIBRATION* trigger);
DWORD XXInputGetState(DWORD index, XINPUT_STATE* state);
DWORD XXInputGetCapabilities(DWORD index, DWORD flags, XINPUT_CAPABILITIES* capabilities);